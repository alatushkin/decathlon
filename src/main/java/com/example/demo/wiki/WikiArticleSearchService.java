package com.example.demo.wiki;

import com.example.demo.ArticleMeta;
import com.example.demo.ArticleSearchService;
import com.example.demo.GeoPoint;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.demo.ServiceException.PermanentException;
import static com.example.demo.ServiceException.TemporaryException;

@RequiredArgsConstructor
@Slf4j
public class WikiArticleSearchService implements ArticleSearchService {

    private static final int LIMIT = 50;
    private final WebClient wikiWebClient;
    private final ObjectMapper objectMapper;

    @Override
    public Mono<List<ArticleMeta>> searchArticleNearPoint(GeoPoint geoPoint) {
        return prepareRequest(geoPoint).retrieve()
                .bodyToMono(String.class)
                .onErrorMap(TemporaryException::new)
                .map(this::deserializeJson)
                .map(this::transformToArticleMeta);
    }


    private WebClient.RequestBodySpec prepareRequest(GeoPoint geoPoint) {
        String uri = "/w/api.php?action=query&format=json&list=geosearch&gscoord=" + toWikiUrlString(geoPoint)
                + "&gsradius=10000&gslimit=" + LIMIT;
        log.trace("Prepare url to request {}", uri);
        return wikiWebClient.post()
                .uri(uri);
    }

    private String toWikiUrlString(GeoPoint geoPoint) {
        return geoPoint.getLat() + "|" + geoPoint.getLng();
    }

    private WikiResponse deserializeJson(String json) {
        try {
            return objectMapper.readValue(json, WikiResponse.class);
        } catch (Exception e) {
            log.error("Cant deserialize wiki response json {}", json);
            throw new PermanentException("Cant deserialize wiki response");
        }
    }

    private List<ArticleMeta> transformToArticleMeta(WikiResponse wikiResponse) {
        return wikiResponse.getQuery().getGeoSearch().stream()
                .map(this::mapWikiItemToArticleMeta)
                .collect(Collectors.toList());
    }

    private ArticleMeta mapWikiItemToArticleMeta(WikiItem wikiItem) {
        return new ArticleMeta(wikiItem.getPageId(), wikiItem.getTitle(), "https://en.wikipedia.org/?curid=" + wikiItem.getPageId());
    }


}