package com.example.demo.wiki;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Value
public class WikiResponseQuery {
    final private List<WikiItem> geoSearch;

    @JsonCreator
    public WikiResponseQuery(@JsonProperty("geosearch") List<WikiItem> geoSearch) {
        this.geoSearch = geoSearch;
    }
}
