package com.example.demo.wiki;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@JsonIgnoreProperties(ignoreUnknown = true)
@Value
public class WikiResponseError {
    private final String error;
    private final String info;

    @JsonCreator
    public WikiResponseError(@JsonProperty("error") String error, @JsonProperty("info") String info) {
        this.error = error;
        this.info = info;
    }
}
