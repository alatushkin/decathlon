package com.example.demo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/articles")
@Slf4j
public class ArticleController {

    @Autowired
    private final ArticleSearchService articleSearchService;

    @GetMapping("/near")
    @CrossOrigin
    private Mono<ResponseEntity<List<ArticleMeta>>> listArticlesNearPoint(@RequestParam Double lat, @RequestParam Double lng) {
        GeoPoint geoPoint = new GeoPoint(lat, lng);
        log.debug("listArticleNearPoint {}", geoPoint);
        return articleSearchService.searchArticleNearPoint(geoPoint)
                .map(ResponseEntity::ok);
    }


    @ExceptionHandler
    public ResponseEntity handlePermanentErrorException(ServiceException.PermanentException ex) {
        return ResponseEntity.status(502).body(ex.getMessage());
    }

    @ExceptionHandler
    public ResponseEntity handleTemporaryErrorException(ServiceException.TemporaryException ex) {
        return ResponseEntity.status(503).body(ex.getMessage());
    }
}
