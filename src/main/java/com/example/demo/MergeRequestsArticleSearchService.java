package com.example.demo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@RequiredArgsConstructor
@Slf4j
public class MergeRequestsArticleSearchService implements ArticleSearchService {
    private static final int MERGE_TTL_SECONDS = 3;
    private final ArticleSearchService delegate;
    private final ConcurrentMap<GeoPoint, Mono<List<ArticleMeta>>> requests = new ConcurrentHashMap<>();

    @Override
    public Mono<List<ArticleMeta>> searchArticleNearPoint(final GeoPoint geoPoint) {
        final Mono<List<ArticleMeta>> response = requests
                .computeIfAbsent(geoPoint, this::delegateCall);
        return removeRequestAfterAllDone(geoPoint, response);
    }


    private Mono<List<ArticleMeta>> delegateCall(GeoPoint point) {
        return delegate.searchArticleNearPoint(point).cache(Duration.ofSeconds(MERGE_TTL_SECONDS));
    }

    private Mono<List<ArticleMeta>> removeRequestAfterAllDone(GeoPoint geoPoint, Mono<List<ArticleMeta>> response) {
        return response.doOnSuccessOrError((articleMetas, throwable) -> requests.remove(geoPoint));
    }
}
