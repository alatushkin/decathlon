package com.example.demo;

public class ServiceException extends RuntimeException {

    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public static class TemporaryException extends ServiceException {

        public TemporaryException() {
        }

        public TemporaryException(String message) {
            super(message);
        }

        public TemporaryException(String message, Throwable cause) {
            super(message, cause);
        }

        public TemporaryException(Throwable cause) {
            super(cause);
        }

        public TemporaryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }

    public static class PermanentException extends ServiceException {

        public PermanentException() {
        }

        public PermanentException(String message) {
            super(message);
        }

        public PermanentException(String message, Throwable cause) {
            super(message, cause);
        }

        public PermanentException(Throwable cause) {
            super(cause);
        }

        public PermanentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }
}
