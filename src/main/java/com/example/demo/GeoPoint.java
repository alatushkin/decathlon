package com.example.demo;

import lombok.Value;

@Value
public class GeoPoint {
    final private Double lat;
    final private Double lng;
}
