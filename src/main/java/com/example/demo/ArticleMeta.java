package com.example.demo;

import lombok.Value;

@Value
public class ArticleMeta {
    private final long pageId;
    private final String title;
    private final String url;
}
