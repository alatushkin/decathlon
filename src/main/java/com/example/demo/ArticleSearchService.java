package com.example.demo;

import reactor.core.publisher.Mono;

import java.util.List;

@FunctionalInterface
public interface ArticleSearchService {
    Mono<List<ArticleMeta>> searchArticleNearPoint(GeoPoint geoPoint);
}
